#include "TRGameInstance.h"
#include "TurnRacer/TurnRacer.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "Engine/Engine.h"
#include "TROnlineSessionEventReceiver.h"

const FName UTRGameInstance::SERVER_NAME_KEY = TEXT("SERVER_NAME");

void UTRGameInstance::SetOnlineSessionEventReceiver(UObject* InOnlineSessionEventReceiver)
{
    const bool bValidChange = (nullptr == InOnlineSessionEventReceiver && nullptr != OnlineSessionEventReceiver) ||
                              (nullptr != InOnlineSessionEventReceiver && nullptr == OnlineSessionEventReceiver);
    if(ensureMsgf(bValidChange,
                  TEXT("Not a valid setting (from %p to %p)! Make sure you clean the receiver before setting a new one!"),
                  OnlineSessionEventReceiver, InOnlineSessionEventReceiver))
    {
        const bool bImplementsInterface = ImplementsInterface<UTROnlineSessionEventReceiver>(InOnlineSessionEventReceiver);

        if(ensureMsgf(nullptr == InOnlineSessionEventReceiver || bImplementsInterface,
                      TEXT("Event receiver must implement 'OnlineSessionEventReceiver' interface!")))
        {
            OnlineSessionEventReceiver = InOnlineSessionEventReceiver;
        }
    }
}

bool UTRGameInstance::CreateSession(const FString& ServerName,
                                    const FString& InPlayerName,
                                    int32 InNumberOfPublicConnections,
                                    EOnlineSubsystemType OnlineSubsystem,
                                    ELevelSize InLevelSize)
{
    if(CheckSessionInterface() && CheckOnlineSessionEventReceiver())
    {
        SessionSettings.bShouldAdvertise = true;
        SessionSettings.bIsLANMatch = OnlineSubsystem == EOnlineSubsystemType::OSST_Lan;
        SessionSettings.bUsesPresence = true;
        SessionSettings.NumPublicConnections = InNumberOfPublicConnections;
        SessionSettings.Set(SERVER_NAME_KEY, ServerName, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);
        OnCreateSessionDelegateHandle = SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UTRGameInstance::OnCreateSessionComplete);
        // Set up member variables
        PlayerName = InPlayerName;
        NumberOfPublicConnections = InNumberOfPublicConnections;
        LevelSize = InLevelSize;

        TR_LOG(NullActor, Log, TEXT("Creating session with server name %s"), *ServerName);
        return SessionInterface->CreateSession(0, NAME_GameSession, SessionSettings);
    }

    return false;
}

bool UTRGameInstance::StartSession()
{
    if(CheckSessionInterface() && CheckOnlineSessionEventReceiver() && CheckSessionCreated())
    {
        OnStartSessionDelegateHandle = SessionInterface->OnStartSessionCompleteDelegates.AddUObject(this, &UTRGameInstance::OnStartSessionComplete);

        return SessionInterface->StartSession(NAME_GameSession);
    }

    return false;
}

bool UTRGameInstance::FindSessions()
{
    if(CheckSessionInterface() && CheckOnlineSessionEventReceiver())
    {
        SessionSearch = MakeShared<FOnlineSessionSearch>();
        SessionSearch->MaxSearchResults = 100;
        SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
        OnFindSessionsDelegateHandle = SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UTRGameInstance::OnFindSessionsComplete);
        TR_LOG(NullActor, Log, TEXT("Finding sessions..."));

        return SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
    }

    return false;
}

bool UTRGameInstance::CancelFindSessions()
{
    if(CheckSessionInterface() && CheckOnlineSessionEventReceiver())
    {
        OnCancelFindSessionsDelegateHandle = SessionInterface->OnCancelFindSessionsCompleteDelegates.AddUObject(this, &UTRGameInstance::OnCancelFindSessionsComplete);
        TR_LOG(NullActor, Log, TEXT("Canceling finding sessions..."));

        return SessionInterface->CancelFindSessions();
    }

    return false;
}

bool UTRGameInstance::Join(const FString& ServerName,
                           const FString& InPlayerName)
{
    if(CheckSessionInterface() && CheckOnlineSessionEventReceiver())
    {
        FOnlineSessionSearchResult* SearchResult = SessionSearch->SearchResults.FindByPredicate([ServerName, this](const FOnlineSessionSearchResult& SearchResult)
        {
            FString FoundServerName;

            if(SearchResult.Session.SessionSettings.Get(SERVER_NAME_KEY, FoundServerName))
            {
                return ServerName == FoundServerName;
            }

            return false;
        });

        if(nullptr != SearchResult)
        {
            OnJoinSessionDelegateHandle = SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UTRGameInstance::OnJoinSessionsComplete);
            PlayerName = InPlayerName;
            TR_LOG(NullActor, Verbose, "Joining session...");

            return SessionInterface->JoinSession(0, NAME_GameSession, *SearchResult);
        }

        return false;
    }

    return false;
}

bool UTRGameInstance::DestroySession()
{
    if(CheckSessionInterface() && CheckOnlineSessionEventReceiver())
    {
        OnDestroySessionDelegateHandle = SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &UTRGameInstance::OnDestroySessionComplete);
        TR_LOG(NullActor, Log, TEXT("Destroying session..."));

        return SessionInterface->DestroySession(NAME_GameSession);
    }

    return false;
}

int32 UTRGameInstance::GetNumberOfPublicConnections() const
{
    return NumberOfPublicConnections;
}

int32 UTRGameInstance::GetNumberOfConnectedPlayers() const
{
    return NumberOfConnectedPlayers;
}

void UTRGameInstance::SetNumberOfConnectedPlayers(int32 InNumberOfConnectedPlayers)
{
    NumberOfConnectedPlayers = InNumberOfConnectedPlayers;
}

const FString& UTRGameInstance::GetPlayerName() const
{
    return PlayerName;
}

void UTRGameInstance::SetPlayerName(const FString& InPlayerName)
{
    PlayerName = InPlayerName;
}

ELevelSize UTRGameInstance::GetLevelSize() const
{
    return LevelSize;
}

void UTRGameInstance::Init()
{
    IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get();
    UEngine* Engine = GetEngine();

    if(ensureMsgf(nullptr != OnlineSubsystem, TEXT("Could not get online subsystem!")))
    {
        SessionInterface = OnlineSubsystem->GetSessionInterface();

        ensureMsgf(SessionInterface.IsValid(), TEXT("Could not get online session interface!"));
    }
    if(nullptr != Engine)
    {
        Engine->OnNetworkFailure().AddUObject(this, &UTRGameInstance::OnNetworkFailure);
    }
    else
    {
        TR_LOG(NullActor, Warning, "Could not set listener for network failures!");
    }
}

void UTRGameInstance::OnNetworkFailure(UWorld* World,
                                       UNetDriver* NetDriver,
                                       ENetworkFailure::Type FailureType,
                                       const FString& ErrorMessage)
{
    OnNetworkFailureDelegate.Broadcast(ErrorMessage);
}

bool UTRGameInstance::CheckSessionInterface() const
{
    return ensureMsgf(SessionInterface.IsValid(), TEXT("No online session interface set!"));
}

bool UTRGameInstance::CheckOnlineSessionEventReceiver() const
{
    if(ensureMsgf(nullptr != OnlineSessionEventReceiver, TEXT("No online session event receiver set!")))
    {
        const bool bImplementsInterface = ImplementsInterface<UTROnlineSessionEventReceiver>(OnlineSessionEventReceiver);

        return ensureMsgf(bImplementsInterface, TEXT("Onlnie session event receiver must implement 'OnlineSessionEventReceiver' interface!"));
    }

    return false;
}

bool UTRGameInstance::CheckSessionCreated() const
{
    if(CheckSessionInterface())
    {
        return ensureMsgf(nullptr != SessionInterface->GetNamedSession(NAME_GameSession), TEXT("Cannot start session without first creating it"));
    }

    return false;
}

void UTRGameInstance::OnCreateSessionComplete(FName SessionName, bool bWasSuccessful)
{
    if(CheckOnlineSessionEventReceiver())
    {
        if(bWasSuccessful)
        {
            TR_LOG(NullActor, Log, "Successfully created session");
            ITROnlineSessionEventReceiver::Execute_OnCreateSessionSuccess(OnlineSessionEventReceiver);
        }
        else
        {
            TR_LOG(NullActor, Warning, "Failed to create session");
            ITROnlineSessionEventReceiver::Execute_OnCreateSessionSuccess(OnlineSessionEventReceiver);
        }
    }
    if(CheckSessionInterface())
    {
        SessionInterface->OnCreateSessionCompleteDelegates.Remove(OnCreateSessionDelegateHandle);
    }
}

void UTRGameInstance::OnStartSessionComplete(FName SessionName, bool bWasSuccessful)
{
    if(CheckOnlineSessionEventReceiver())
    {
        if(bWasSuccessful)
        {
            TR_LOG(NullActor, Log, "Successfully started session");
            ITROnlineSessionEventReceiver::Execute_OnStartSessionSuccess(OnlineSessionEventReceiver);
        }
        else
        {
            TR_LOG(NullActor, Warning, "Failed to start session");
            ITROnlineSessionEventReceiver::Execute_OnStartSessionFailure(OnlineSessionEventReceiver);
        }
    }
    if(CheckSessionInterface())
    {
        SessionInterface->OnStartSessionCompleteDelegates.Remove(OnStartSessionDelegateHandle);
    }
}

void UTRGameInstance::OnFindSessionsComplete(bool bWasSuccessful)
{
    if(CheckOnlineSessionEventReceiver())
    {
        if(bWasSuccessful)
        {
            TArray<FString> ServerNames;

            TR_LOG(NullActor, Log, "Successfully found sessions");
            for(const FOnlineSessionSearchResult& SearchResult: SessionSearch->SearchResults)
            {
                FString ServerName;

                if(SearchResult.Session.SessionSettings.Get(SERVER_NAME_KEY, ServerName))
                {
                    ServerNames.Add(ServerName);
                }
            }
            ITROnlineSessionEventReceiver::Execute_OnFindSessionsSuccess(OnlineSessionEventReceiver, ServerNames);
        }
        else
        {
            TR_LOG(NullActor, Warning, "Failed to find sessions");
            ITROnlineSessionEventReceiver::Execute_OnFindSessionsFailure(OnlineSessionEventReceiver);
        }
    }
    if(CheckSessionInterface())
    {
        SessionInterface->OnFindSessionsCompleteDelegates.Remove(OnFindSessionsDelegateHandle);
    }
}

void UTRGameInstance::OnCancelFindSessionsComplete(bool bWasSuccessful)
{
    if(CheckOnlineSessionEventReceiver())
    {
        if(bWasSuccessful)
        {
            TR_LOG(NullActor, Log, "Successfully cancelled finding sessions");
            ITROnlineSessionEventReceiver::Execute_OnCancelFindSessionsSuccess(OnlineSessionEventReceiver);
        }
        else
        {
            TR_LOG(NullActor, Warning, "Failed to cancel finding sessions");
            ITROnlineSessionEventReceiver::Execute_OnCancelFindSessionsFailure(OnlineSessionEventReceiver);
        }
    }
    if(CheckSessionInterface())
    {
        SessionInterface->OnCancelFindSessionsCompleteDelegates.Remove(OnCancelFindSessionsDelegateHandle);
    }
}

void UTRGameInstance::OnJoinSessionsComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
    if(CheckSessionInterface())
    {
        if(CheckOnlineSessionEventReceiver())
        {
            if (Result == EOnJoinSessionCompleteResult::Success)
            {
                FString ConnectionString;

                if(SessionInterface->GetResolvedConnectString(NAME_GameSession, ConnectionString))
                {
                    TR_LOG(NullActor, Log, "Successfully joined session");
                    ITROnlineSessionEventReceiver::Execute_OnJoinSessionSuccess(OnlineSessionEventReceiver, ConnectionString);
                }
                else
                {
                    TR_LOG(NullActor, Warning, "Failed to get connection string");
                    ITROnlineSessionEventReceiver::Execute_OnJoinSessionFailure(OnlineSessionEventReceiver);
                }
            }
            else
            {
                TR_LOG(NullActor, Warning, "Failed to join session");
                ITROnlineSessionEventReceiver::Execute_OnJoinSessionFailure(OnlineSessionEventReceiver);
            }
        }
        SessionInterface->OnJoinSessionCompleteDelegates.Remove(OnJoinSessionDelegateHandle);
    }
}

void UTRGameInstance::OnDestroySessionComplete(FName SessionName, bool bWasSuccessful)
{
    if(CheckOnlineSessionEventReceiver())
    {
        if(bWasSuccessful)
        {
            TR_LOG(NullActor, Log, "Successfully destroyed session");
            ITROnlineSessionEventReceiver::Execute_OnDestroySessionSuccess(OnlineSessionEventReceiver);
        }
        else
        {
            TR_LOG(NullActor, Warning, "Failed to destroy session");
            ITROnlineSessionEventReceiver::Execute_OnDestroySessionFailure(OnlineSessionEventReceiver);
        }
    }
    if(CheckSessionInterface())
    {
        SessionInterface->OnDestroySessionCompleteDelegates.Remove(OnDestroySessionDelegateHandle);
    }
}
