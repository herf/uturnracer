#include "GameLevel/TRGameLevelGameMode.h"

#include "EngineUtils.h"
#include "TRGameInstance.h"
#include "GameLevel/TRAIController.h"
#include "Engine/World.h"
#include "GameLevel/TRLevelGenerator.h"
#include "Kismet/KismetMathLibrary.h"

// TODO: Possible game mechanics:
//       At the beginning everyone needs to pick up the middle point
//       Then all the start positions need to be reached
//       And to finish the game, the middle point needs to be reached again

void ATRGameLevelGameMode::BeginPlay()
{
    Super::BeginPlay();
    UWorld* World = GetWorld();
    UTRGameInstance* GameInstance = GetGameInstance<UTRGameInstance>();

    if(ensureMsgf(nullptr != World, TEXT("Failed to get World")) &&
       ensureMsgf(nullptr != GameInstance, TEXT("Failed to get game instance")))
    {
        LevelGenerator = World->SpawnActor<ATRLevelGenerator>();
        if(ensureMsgf(nullptr != LevelGenerator, TEXT("Failed to create LevelGenerator")))
        {
            LevelGenerator->SetLevelSize(GameInstance->GetLevelSize());
            LevelGenerator->SetRoadSize(RoadSize);
            LevelGenerator->SetRefineLevel(RefineLevel);
            LevelGenerator->SetMaterial(LevelMaterial);
            LevelGenerator->GenerateLevel();
            for(const FVector& StartPosition: LevelGenerator->GetStartPositions())
            {
                PlayerPositions.Add(StartPosition, nullptr);
            }
        }

        // Create the start positions and the middle point
        if(nullptr != StartPositionClass)
        {
            // Start positions
            for(const auto& PlayerPosition: PlayerPositions)
            {
                AActor* StartPosition = World->SpawnActor(StartPositionClass);

                StartPosition->SetActorLocation(PlayerPosition.Key);
            }
            // Middle point
            AActor* MiddlePosition = World->SpawnActor(StartPositionClass);

            MiddlePosition->SetActorLocation(LevelGenerator->GetMiddlePoint());
        }

        // Create bots and place them randomly
        const uint32 NumberOfBots = GameInstance->GetNumberOfPublicConnections() - GameInstance->GetNumberOfConnectedPlayers();
        TArray<FVector> PlayerPositionsKeys;

        PlayerPositions.GetKeys(PlayerPositionsKeys);
        if(ensureMsgf(nullptr != PlayerClass, TEXT("Playerclass must be set!")))
        {
            // Spawn the bots
            if(ensureMsgf(nullptr != AiControllerClass, TEXT("AiController must be set!")))
            {
                for(uint32 Index = 0; Index < NumberOfBots; ++Index)
                {
                    const int32 SelectedIndex = FMath::RandRange(0, PlayerPositionsKeys.Num() - 1);
                    const FVector SelectedPosition = PlayerPositionsKeys[SelectedIndex];
                    APawn* Bot = World->SpawnActor<APawn>(PlayerClass);
                    ATRAIController* AiController = World->SpawnActor<ATRAIController>(AiControllerClass);

                    PlayerPositionsKeys.RemoveAt(SelectedIndex);
                    // Place the actor above the starting position
                    Bot->SetActorLocation(SelectedPosition + FVector(0.0f, 0.0f, 50.0f));
                    Bot->SetActorRotation(GetPawnRotation(Bot));
                    AiController->Possess(Bot);
                    PlayerPositions[SelectedPosition] = Bot;
                }
            }
            
            // Spawn pawns for already logged players (There should be only one, the hosting player)
            int32 PlayerCounter = 0;

            for(TActorIterator<APlayerController> PlayerIterator(World); PlayerIterator; ++PlayerIterator)
            {
                const int32 SelectedIndex = FMath::RandRange(0, PlayerPositionsKeys.Num() - 1);
                const FVector SelectedPosition = PlayerPositionsKeys[SelectedIndex];
                APawn* Pawn = World->SpawnActor<APawn>(PlayerClass);

                // TODO: Common method to spawn a Pawn
                PlayerPositionsKeys.RemoveAt(SelectedIndex);
                // Place the actor above the starting position
                Pawn->SetActorLocation(SelectedPosition + FVector(0.0f, 0.0f, 50.0f));
                Pawn->SetActorRotation(GetPawnRotation(Pawn));
                PlayerIterator->Possess(Pawn);
                PlayerPositions[SelectedPosition] = Pawn;
                checkf(PlayerCounter == 0, TEXT("More than one already logged player found!"));
                ++PlayerCounter;
            }
        }
    }
}

void ATRGameLevelGameMode::PostLogin(APlayerController* NewPlayer)
{
    Super::PostLogin(NewPlayer);
    TArray<FVector> FreeStartPositions;

    for(const auto& PlayerPosition: PlayerPositions)
    {
        if(nullptr == PlayerPosition.Value)
        {
            FreeStartPositions.Add(PlayerPosition.Key);
        }
    }

    if(ensureMsgf(0 != FreeStartPositions.Num(), TEXT("No more free starting position for the new player!")))
    {
        const int32 SelectedStartPositionIndex = FMath::RandRange(0, FreeStartPositions.Num() - 1);
        const FVector SelectedStartPosition = FreeStartPositions[SelectedStartPositionIndex];
        UWorld* World = GetWorld();

        if(ensureMsgf(nullptr != PlayerClass, TEXT("Player class must be set!")) &&
           ensureMsgf(nullptr != World, TEXT("Could not get world")))
        {
            APawn* Pawn = World->SpawnActor<APawn>(PlayerClass);

            if(ensureMsgf(nullptr != Pawn, TEXT("Could not spawn player!")))
            {
                // Place the actor above the starting point
                Pawn->SetActorLocation(SelectedStartPosition + FVector(0.0f, 0.0f, 50.0f));
                PlayerPositions[SelectedStartPosition] = Pawn;
                Pawn->SetActorRotation(GetPawnRotation(Pawn));;
                NewPlayer->Possess(Pawn);
            }
        }
    }
}

FRotator ATRGameLevelGameMode::GetPawnRotation(APawn* Pawn) const
{
    // Each pawn looks towards the middle point
    // FIXME: Why it is working only if I set the Start to the MiddlePoint and Target to the SelectedPosition?
    //        Feels like it should work the opposite
    FRotator Rotator = UKismetMathLibrary::FindLookAtRotation(LevelGenerator->GetMiddlePoint(), Pawn->GetActorLocation());
    const float Yaw = Rotator.Yaw < 0 ? 360.0f + Rotator.Yaw : Rotator.Yaw; 
    const int32 Multiplier = FMath::RoundToInt(Yaw) / 45;
    const int32 Lower = Multiplier * 45;
    const int32 Upper = (Multiplier + 1) * 45;
    const int32 DiffFromLower = Yaw - Lower;
    const int32 DiffFromUpper = Upper - Yaw;

    if(DiffFromLower < DiffFromUpper)
    {
        Rotator.Yaw =  Lower;
    }
    else
    {
        Rotator.Yaw = Upper;
    }
    // Others should be zero
    Rotator.Pitch = 0;
    Rotator.Roll = 0;

    return Rotator;
}
