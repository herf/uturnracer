#include "GameLevel/TRLevelGenerator.h"
#include "ProceduralMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "TurnRacer/TurnRacer.h"

ATRLevelGenerator::ATRLevelGenerator()
{
    PrimaryActorTick.bCanEverTick = true;
    SetReplicates(true);
    Mesh = CreateDefaultSubobject<UProceduralMeshComponent>("Mesh");
    if(nullptr == RootComponent)
    {
        RootComponent = Mesh;
    }
    else
    {
        Mesh->SetupAttachment(RootComponent);
    }
}

ELevelSize ATRLevelGenerator::GetLevelSize() const
{
    return LevelSize;
}

void ATRLevelGenerator::SetLevelSize(ELevelSize InLevelSize)
{
    LevelSize = InLevelSize;
}

int32 ATRLevelGenerator::GetRoadSize() const
{
    return RoadSize;
}

void ATRLevelGenerator::SetRoadSize(int32 InRoadSize)
{
    RoadSize = InRoadSize;
}

int32 ATRLevelGenerator::GetRefineLevel() const
{
    return RefineLevel;
}

void ATRLevelGenerator::SetRefineLevel(int32 InRefineLevel)
{
    RefineLevel = InRefineLevel;
}

UMaterialInterface* ATRLevelGenerator::GetMaterial() const
{
    return Material;
}

void ATRLevelGenerator::SetMaterial(UMaterialInterface* InMaterial)
{
    Material = InMaterial;
}

void ATRLevelGenerator::GenerateLevel()
{
    const int32 Size = GetSize();

    Level.SetNum(Size);
    for(TArray<bool>& Array: Level)
    {
        Array.SetNum(Size);
    }
    SetPoints();
    // Generate level between the starting points
    for(int32 i = 0; i < RefineLevel; ++i)
    {
        for(int32 StartPointIndex = 0; StartPointIndex < StartPoints.Num(); ++StartPointIndex)
        {
            const int32 NextIndex = StartPointIndex == StartPoints.Num() - 1 ? 0 : StartPointIndex + 1;
            
            GenerateLevel(StartPoints[StartPointIndex], StartPoints[NextIndex]);
        }
    }
    // Generate level between all the starting points and the middle point
    for(int32 i = 0; i < RefineLevel; ++i)
    {
        for(int32 StartPointIndex = 0; StartPointIndex < StartPoints.Num(); ++StartPointIndex)
        {
            GenerateLevel(StartPoints[StartPointIndex], MiddlePoint);
        }
    }

    // Time to replicate the generated level
    for(auto& Column: Level)
    {
        FReplicatedLevelColumn ReplicatedColumn;

        ReplicatedColumn.LevelColumn = Column;
        ReplicatedLevel.Level.Add(ReplicatedColumn);
    }

    CreateMesh();
}

TArray<FVector> ATRLevelGenerator::GetStartPositions() const
{
    TArray<FVector> StartPositions;

    for(const FVector2D& StartPoint: StartPoints)
    {
        StartPositions.Emplace(StartPoint.X * 100.0f, StartPoint.Y * 100.0f, 50.0f);
    }

    return StartPositions;
}

FVector ATRLevelGenerator::GetMiddlePoint() const
{
    return FVector(MiddlePoint.X * 100.0f, MiddlePoint.Y * 100.0f, 50.0f);
}

void ATRLevelGenerator::BeginPlay()
{
    Super::BeginPlay();
}

void ATRLevelGenerator::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void ATRLevelGenerator::OnRep_ReplicatedLevel()
{
    if(ensureMsgf(!HasAuthority(), TEXT("Received replicated level on server")))
    {
        for(auto& ReplicatedLevelColumn: ReplicatedLevel.Level)
        {
            Level.Add(ReplicatedLevelColumn.LevelColumn);
        }
        // If the material is also replicated, we are ready to create the mesh
        if(nullptr != Material)
        {
            CreateMesh();
        }
    }
}

void ATRLevelGenerator::OnRep_Material()
{
    if(ensureMsgf(!HasAuthority(), TEXT("Received replicated material on server")))
    {
        // If the level has also replicated we are ready to create the mesh
        if(0 != Level.Num())
        {
            CreateMesh();
        }
    }
}

void ATRLevelGenerator::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    DOREPLIFETIME(ATRLevelGenerator, ReplicatedLevel);
    DOREPLIFETIME(ATRLevelGenerator, Material);
}

int32 ATRLevelGenerator::GetSize() const
{
    switch(LevelSize)
    {
    case ELevelSize::LS_Small:
        return 33;
    case ELevelSize::LS_Medium:
        return 66;
    case ELevelSize::LS_Large:
        return 132;
    default:
        TR_LOG(this, Error, TEXT("Unknown level size: %d"), static_cast<int32>(LevelSize));
    }

    return 0;
}

void ATRLevelGenerator::GenerateLevel(const FVector2D& From, const FVector2D& To)
{
    FVector2D Current = From;
    float Distance = FVector2D::DistSquared(Current, To);
    int32 Counter = 0;
    const int32 Size = GetSize();

    AddToLevel(From);
    AddToLevel(To);
    while(Current != To && Distance > 2 && Counter < Size * 4)
    {
        Current = CalculateNewPosition(Current, To, true);

        Distance = FVector2D::DistSquared(Current, To);
        ++Counter;
    }
    while(Current != To)
    {
        // Adjust the level, by directly going to the destination
        Current = CalculateNewPosition(Current, To, false);
    }
}

void ATRLevelGenerator::AddToLevel(const FVector2D& Position)
{
    Level[Position.X][Position.Y] = true;
    // Make the neighbors part of the level too
    for(uint32 X = static_cast<uint32>(Position.X) - RoadSize; X < static_cast<uint32>(Position.X) + RoadSize + 1; ++X)
    {
        for(uint32 Y = static_cast<uint32>(Position.Y) - RoadSize; Y < static_cast<uint32>(Position.Y) + RoadSize + 1; ++Y)
        {
            if(IsValid({static_cast<float>(X), static_cast<float>(Y)}))
            {
                Level[X][Y] = true;
            }
        }
    }
}

FVector2D ATRLevelGenerator::Round(const FVector2D& Vector) const
{
    return { FMath::RoundHalfToZero(Vector.X), FMath::RoundHalfToZero(Vector.Y) };
}

bool ATRLevelGenerator::IsValid(const FVector2D& Position) const
{
    const int32 Size = GetSize();

    return Position.X >= 1 && Position.X < Size - 1 &&
           Position.Y >= 1 && Position.Y < Size - 1;
}

FVector2D ATRLevelGenerator::CalculateNewPosition(const FVector2D& Current, const FVector2D& To, bool bRandomize)
{
    const FVector2D Direction = (To - Current).GetSafeNormal();
    const FVector2D Random = bRandomize ? FVector2D(FMath::VRand()) : FVector2D::ZeroVector;
    const FVector2D NewPosition = Current + Round(Random + Direction);

    if(IsValid(NewPosition))
    {
        AddToLevel(NewPosition);

        return NewPosition;
    }

    return Current;
}

void ATRLevelGenerator::CreateMesh()
{
    TArray<FVector> Vertices;
    TArray<int32> Indices;
    TArray<FVector> Normals;
    TArray<FVector2D> Uvs;
    TArray<FColor> VertexColours;
    const int32 Size = GetSize();
    const float UvSteps = 1;//1.0f / Size;
    const uint32 NumberOfVertices = (Size * Size) + ((Size - 1) * (Size - 1));
    const uint32 NumberOfIndices = (Size - 1) * (Size - 1) * 4 * 3;

    Vertices.Reserve(NumberOfVertices);
    Uvs.Reserve(NumberOfVertices);
    VertexColours.AddZeroed(NumberOfVertices);
    Indices.Reserve(NumberOfIndices);
    for(int32 X = 0; X < Level.Num() - 1; ++X)
    {
        for(int32 Y = 0; Y < Level[X].Num() - 1; ++Y)
        {
            // We are going to add four triangles here
            // C---D
            // |\ /|
            // | E |
            // |/ \|
            // A   B
            const FVector VertexA(X * 100,          Y * 100,          Level[X]    [Y]     ? 50 : 150);
            const FVector VertexB((X + 1) * 100,    Y * 100,          Level[X + 1][Y]     ? 50 : 150);
            const FVector VertexC(X * 100,          (Y + 1) * 100,    Level[X]    [Y + 1] ? 50 : 150);
            const FVector VertexD((X + 1) * 100,    (Y + 1) * 100,    Level[X + 1][Y + 1] ? 50 : 150);
            const FVector VertexE((X + 0.5f) * 100, (Y + 0.5f) * 100, (VertexA.Z + VertexB.Z + VertexC.Z + VertexD.Z) / 4.0f);
            const uint32 IndexA = Vertices.AddUnique(VertexA);
            const uint32 IndexB = Vertices.AddUnique(VertexB);
            const uint32 IndexC = Vertices.AddUnique(VertexC);
            const uint32 IndexD = Vertices.AddUnique(VertexD);
            const uint32 IndexE = Vertices.AddUnique(VertexE);

            Indices.Append(TArray<uint32>({IndexA, IndexE, IndexB}));
            Indices.Append(TArray<uint32>({IndexA, IndexC, IndexE}));
            Indices.Append(TArray<uint32>({IndexC, IndexD, IndexE}));
            Indices.Append(TArray<uint32>({IndexD, IndexB, IndexE}));

            // TODO: Here we could use the indexes
            Uvs.AddUnique(FVector2D(X * UvSteps,          Y * UvSteps));
            Uvs.AddUnique(FVector2D((X + 1) * UvSteps,    Y * UvSteps));
            Uvs.AddUnique(FVector2D(X * UvSteps,          (Y + 1) * UvSteps));
            Uvs.AddUnique(FVector2D((X + 1) * UvSteps,    (Y + 1) * UvSteps));
            Uvs.AddUnique(FVector2D((X + 0.5f) * UvSteps, (Y + 0.5f) * UvSteps));

            // Setting up the vertex colours here does not work, for whatever reason on the client
            // So the material is changed, to get the needed data from the vertex positions
            // Would be better to have these constants (50 and 100) in one place, but yeah...
            // VertexColours[IndexA] = FColor(FMath::Lerp(0, 255, (VertexA.Z - 50) / 100), 0, 0);
            // VertexColours[IndexB] = FColor(FMath::Lerp(0, 255, (VertexB.Z - 50) / 100), 0, 0);
            // VertexColours[IndexC] = FColor(FMath::Lerp(0, 255, (VertexC.Z - 50) / 100), 0, 0);
            // VertexColours[IndexD] = FColor(FMath::Lerp(0, 255, (VertexD.Z - 50) / 100), 0, 0);
            // VertexColours[IndexE] = FColor(FMath::Lerp(0, 255, (VertexE.Z - 50) / 100), 0, 0);
        }
    }

    Mesh->CreateMeshSection(0, Vertices, Indices, Normals, Uvs, VertexColours, TArray<FProcMeshTangent>(), true);
    Mesh->Mobility = EComponentMobility::Movable;
    if(nullptr != Material)
    {
        Mesh->SetMaterial(0, Material);
    }
}

void ATRLevelGenerator::SetPoints()
{
    const int32 Size = GetSize();
    const int32 ThirdSize = Size / 3;

    StartPoints.AddZeroed(8);
    for(int32 X = 0; X < 3; ++X)
    {
        for(int32 Y = 0; Y < 3; ++Y)
        {
            const int32 MinX = FMath::Max(2 + RoadSize, 0 + (X * ThirdSize) - 2 - RoadSize);
            const int32 MaxX = (X + 1) * ThirdSize - 2 - RoadSize;
            const int32 MinY = FMath::Max(2 + RoadSize, 0 + (Y * ThirdSize) - 2 - RoadSize);
            const int32 MaxY = (Y + 1) * ThirdSize - 2 - RoadSize;
            const FVector2D Point = FVector2D(FMath::RandRange(MinX, MaxX), FMath::RandRange(MinY, MaxY)); 

            if(0 == X && 0 == Y)
            {
                StartPoints[0] = Point;
            }
            else if(0 == X && 1 == Y)
            {
                StartPoints[1] = Point;
            }
            else if(0 == X && 2 == Y)
            {
                StartPoints[2] = Point;
            }
            else if(1 == X && 2 == Y)
            {
                StartPoints[3] = Point;
            }
            else if(2 == X && 2 == Y)
            {
                StartPoints[4] = Point;
            }
            else if(2 == X && 1 == Y)
            {
                StartPoints[5] = Point;
            }
            else if(2 == X && 0 == Y)
            {
                StartPoints[6] = Point;
            }
            else if(1 == X && 0 == Y)
            {
                StartPoints[7] = Point;
            }
            else if(1 == X && 1 == Y)
            {
                MiddlePoint = Point;
            }
        }
    }
}
