#include "UI/MainMenu/TRMainMenuUserWidget.h"
#include "TRGameInstance.h"
#include "GameFramework/PlayerState.h"
#include "TurnRacer/TurnRacer.h"

void UTRMainMenuUserWidget::Host(const FString& ServerName,
                                 const FString& PlayerName,
                                 int32 NumberOfPublicConnections,
                                 EOnlineSubsystemType OnlineSubsystem,
                                 ELevelSize LevelSize)
{
    UTRGameInstance* GameInstance = GetGameInstance<UTRGameInstance>();

    if(nullptr != GameInstance)
    {
        SetOnlineSessionEventReceiver();
        if(!GameInstance->CreateSession(ServerName, PlayerName, NumberOfPublicConnections, OnlineSubsystem, LevelSize))
        {
            ResetOnlineSessionEventReceiver();
            BP_OnHostFailed();
        }
    }
    else
    {
        BP_OnHostFailed();
    }
}

void UTRMainMenuUserWidget::FindSessions()
{
    UTRGameInstance* GameInstance = GetGameInstance<UTRGameInstance>();

    if(nullptr != GameInstance)
    {
        SetOnlineSessionEventReceiver();
        if(!GameInstance->FindSessions())
        {
            ResetOnlineSessionEventReceiver();
            BP_OnFindSessionsFailed();
        }
    }
    else
    {
        BP_OnFindSessionsFailed();
    }
}

void UTRMainMenuUserWidget::CancelFindSessions()
{
    UTRGameInstance* GameInstance = GetGameInstance<UTRGameInstance>();

    if(nullptr != GameInstance)
    {
        SetOnlineSessionEventReceiver();
        if(!GameInstance->CancelFindSessions())
        {
            ResetOnlineSessionEventReceiver();
            BP_OnCancelFindSessionsFailed();
        }
    }
    else
    {
        BP_OnCancelFindSessionsFailed();
    }
}

void UTRMainMenuUserWidget::JoinSession(const FString& ServerName,
                                        const FString& PlayerName)
{
    UTRGameInstance* GameInstance = GetGameInstance<UTRGameInstance>();

    if(nullptr != GameInstance)
    {
        SetOnlineSessionEventReceiver();
        if(!GameInstance->Join(ServerName, PlayerName))
        {
            ResetOnlineSessionEventReceiver();
            BP_OnJoinSessionFailed();
        }
    }
    else
    {
        BP_OnJoinSessionFailed();
    }
}

void UTRMainMenuUserWidget::DestroySession()
{
    UTRGameInstance* GameInstance = GetGameInstance<UTRGameInstance>();

    if(nullptr != GameInstance)
    {
        SetOnlineSessionEventReceiver();
        if(!GameInstance->DestroySession())
        {
            ResetOnlineSessionEventReceiver();
            BP_OnDestroySessionFailed();
        }
    }
    else
    {
        BP_OnDestroySessionFailed();
    }
}

void UTRMainMenuUserWidget::OnCreateSessionSuccess_Implementation()
{
    UWorld* World = GetWorld();

    ResetOnlineSessionEventReceiver();
    if(nullptr != World)
    {
        BP_OnHostSucceeded();
        World->ServerTravel("/Game/Levels/LobbyLevel?listen");
    }
    else
    {
        BP_OnHostFailed();
    }
}

void UTRMainMenuUserWidget::OnCreateSessionFailure_Implementation()
{
    ResetOnlineSessionEventReceiver();
    BP_OnHostFailed();
}

void UTRMainMenuUserWidget::OnStartSessionSuccess_Implementation()
{
    TR_LOG(GetOwningPlayer(), Error, TEXT("Invalid callback 'OnStartSessionSuccess' received on main menu"));
}

void UTRMainMenuUserWidget::OnStartSessionFailure_Implementation()
{
    TR_LOG(GetOwningPlayer(), Error, TEXT("Invalid callback 'OnStartSessionFailure' received on main menu"));
}

void UTRMainMenuUserWidget::OnFindSessionsSuccess_Implementation(const TArray<FString>& ServerNames)
{
    ResetOnlineSessionEventReceiver();
    BP_OnFindSessionsSucceeded(ServerNames);
}

void UTRMainMenuUserWidget::OnFindSessionsFailure_Implementation()
{
    ResetOnlineSessionEventReceiver();
    BP_OnFindSessionsFailed();
}

void UTRMainMenuUserWidget::OnCancelFindSessionsSuccess_Implementation()
{
    ResetOnlineSessionEventReceiver();
    BP_OnCancelFindSessionsSucceeded();
}

void UTRMainMenuUserWidget::OnCancelFindSessionsFailure_Implementation()
{
    ResetOnlineSessionEventReceiver();
    BP_OnCancelFindSessionsFailed();
}

void UTRMainMenuUserWidget::OnJoinSessionSuccess_Implementation(const FString& ConnectionString)
{
    APlayerController* PlayerController = GetOwningPlayer();

    ResetOnlineSessionEventReceiver();
    if(nullptr != PlayerController)
    {
        BP_OnJoinSessionSucceeded();
        PlayerController->ClientTravel(ConnectionString, ETravelType::TRAVEL_Absolute);
    }
    else
    {
        BP_OnJoinSessionFailed();
    }
}

void UTRMainMenuUserWidget::OnJoinSessionFailure_Implementation()
{
    ResetOnlineSessionEventReceiver();
    BP_OnJoinSessionFailed();
}

void UTRMainMenuUserWidget::OnDestroySessionSuccess_Implementation()
{
    ResetOnlineSessionEventReceiver();
    BP_OnDestroySessionSucceeded();
}

void UTRMainMenuUserWidget::OnDestroySessionFailure_Implementation()
{
    ResetOnlineSessionEventReceiver();
    BP_OnDestroySessionFailed();
}

void UTRMainMenuUserWidget::SetOnlineSessionEventReceiver()
{
    if(!bIsOnlineSessionEventReceiverSet)
    {
        if(SetOnlineSessionEventReceiver(this))
        {
            bIsOnlineSessionEventReceiverSet = true;
        }
        
    }
}

void UTRMainMenuUserWidget::ResetOnlineSessionEventReceiver()
{
    if(bIsOnlineSessionEventReceiverSet)
    {
        if(SetOnlineSessionEventReceiver(nullptr))
        {
            bIsOnlineSessionEventReceiverSet = false;
        }
    }
}

bool UTRMainMenuUserWidget::SetOnlineSessionEventReceiver(UTRMainMenuUserWidget* OnlineSessionEventReceiver)
{
    UTRGameInstance* GameInstance = GetGameInstance<UTRGameInstance>();

    if(nullptr != GameInstance)
    {
        GameInstance->SetOnlineSessionEventReceiver(OnlineSessionEventReceiver);

        return true;
    }

    return false;
}
