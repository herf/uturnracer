#include "UI/TRButtonUserWidget.h"
#include "Components/TextBlock.h"

void UTRButtonUserWidget::SetText(const FText& InText)
{
    Text = InText;
    TextBlock->SetText(Text);
}

void UTRButtonUserWidget::NativePreConstruct()
{
    TextBlock->SetText(Text);
    if(!Button->OnClicked.IsAlreadyBound(this, &UTRButtonUserWidget::OnClick))
    {
        Button->OnClicked.AddDynamic(this, &UTRButtonUserWidget::OnClick);
    }
    if(!Button->OnPressed.IsAlreadyBound(this, &UTRButtonUserWidget::OnPress))
    {
        Button->OnPressed.AddDynamic(this, &UTRButtonUserWidget::OnPress);
    }
    if(!Button->OnReleased.IsAlreadyBound(this, &UTRButtonUserWidget::OnRelease))
    {
        Button->OnReleased.AddDynamic(this, &UTRButtonUserWidget::OnRelease);
    }
    if(!Button->OnHovered.IsAlreadyBound(this, &UTRButtonUserWidget::OnHover))
    {
        Button->OnHovered.AddDynamic(this, &UTRButtonUserWidget::OnHover);
    }
    if(!Button->OnUnhovered.IsAlreadyBound(this, &UTRButtonUserWidget::OnUnhover))
    {
        Button->OnUnhovered.AddDynamic(this, &UTRButtonUserWidget::OnUnhover);
    }
}

void UTRButtonUserWidget::OnClick()
{
    OnClicked.Broadcast();
}

void UTRButtonUserWidget::OnPress()
{
    OnPressed.Broadcast();
}

void UTRButtonUserWidget::OnRelease()
{
    OnReleased.Broadcast();
}

void UTRButtonUserWidget::OnHover()
{
    OnHovered.Broadcast();
}

void UTRButtonUserWidget::OnUnhover()
{
    OnUnhovered.Broadcast();
}