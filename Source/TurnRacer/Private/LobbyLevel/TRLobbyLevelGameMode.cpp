#include "LobbyLevel/TRLobbyLevelGameMode.h"
#include "TRGameInstance.h"
#include "GameFramework/GameSession.h"
#include "GameFramework/PlayerState.h"
#include "Engine/World.h"
#include "LobbyLevel/TRLobbyLevelPlayerState.h"
#include "TurnRacer/TurnRacer.h"

ATRLobbyLevelGameMode::ATRLobbyLevelGameMode()
{
    PlayerStateClass = ATRLobbyLevelPlayerState::StaticClass();
}

void ATRLobbyLevelGameMode::PlayerReady()
{
    if(!IsAllPlayersReady())
    {
        return;
    }
    if(0 == Bots.Num())
    {
        // The maximum number of players connected, and they are all ready.
        // Time to start the game
        StartGame();
    }
}

void ATRLobbyLevelGameMode::StartGame()
{
    UTRGameInstance* GameInstance = GetGameInstance<UTRGameInstance>();

    if(ensureMsgf(nullptr != GameInstance, TEXT("Failed to get game instance")))
    {
        // Inform everyone that the game is about to start
        for(ATRLobbyLevelPlayerController* Player: Players)
        {
            Player->Client_OnGameStarts();
        }

        SetOnlineSessionEventReceiver();

        if(!GameInstance->StartSession())
        {
            ResetOnlineSessionEventReceiver();
            // Inform players that we failed to start the game
            for(ATRLobbyLevelPlayerController* Player: Players)
            {
                Player->Client_OnGameStartFailed();
            }
        }
    }
}

void ATRLobbyLevelGameMode::BeginPlay()
{
    CreateBots();
}

//
// ITROnlineSessionEventReceiver interface
//

void ATRLobbyLevelGameMode::OnCreateSessionSuccess_Implementation()
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnCreateSessionSuccess' received on lobby level game mode"));
}

void ATRLobbyLevelGameMode::OnCreateSessionFailure_Implementation()
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnCreateSessionFailure' received on lobby level game mode"));
}

void ATRLobbyLevelGameMode::OnStartSessionSuccess_Implementation()
{
    UWorld* World = GetWorld();

    ResetOnlineSessionEventReceiver();
    if(nullptr != World)
    {
        UTRGameInstance* GameInstance = GetGameInstance<UTRGameInstance>();

        if(ensureMsgf(nullptr != GameInstance, TEXT("Could not get game instance")))
        {
            GameInstance->SetNumberOfConnectedPlayers(Players.Num());
        }
        World->ServerTravel("/Game/Levels/GameLevel?listen");
    }
}

void ATRLobbyLevelGameMode::OnStartSessionFailure_Implementation()
{
    ResetOnlineSessionEventReceiver();
    // Inform players that we failed to start the game
    for(ATRLobbyLevelPlayerController* Player: Players)
    {
        Player->Client_OnGameStartFailed();
    }
}

void ATRLobbyLevelGameMode::OnFindSessionsSuccess_Implementation(const TArray<FString>& ServerNames)
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnFindSessionsSuccess' received on lobby level game mode"));
}

void ATRLobbyLevelGameMode::OnFindSessionsFailure_Implementation()
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnFindSessionsFailure' received on lobby level game mode"));
}

void ATRLobbyLevelGameMode::OnCancelFindSessionsSuccess_Implementation()
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnCancelFindSessionsSuccess' received on lobby level game mode"));
}

void ATRLobbyLevelGameMode::OnCancelFindSessionsFailure_Implementation()
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnCancelFindSessionsFailure' received on lobby level game mode"));
}

void ATRLobbyLevelGameMode::OnJoinSessionSuccess_Implementation(const FString& ConnectionString)
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnJoinSessionSuccess' received on lobby level game mode"));
}

void ATRLobbyLevelGameMode::OnJoinSessionFailure_Implementation()
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnJoinSessionFailure' received on lobby level game mode"));
}

void ATRLobbyLevelGameMode::OnDestroySessionSuccess_Implementation()
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnDestroySessionSuccess' received on lobby level game mode"));
}

void ATRLobbyLevelGameMode::OnDestroySessionFailure_Implementation()
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnDestroySessionFailure' received on lobby level game mode"));
}

//
// AGameModeBase overrides
//

void ATRLobbyLevelGameMode::PreLogin(const FString& Options,
                                     const FString& Address,
                                     const FUniqueNetIdRepl& UniqueId,
                                     FString& ErrorMessage)
{
    Super::PreLogin(Options, Address, UniqueId, ErrorMessage);

    // Only do anything if Super did not signal any error
    if(ErrorMessage.IsEmpty())
    {
        UTRGameInstance* GameInstance = GetGameInstance<UTRGameInstance>();

        if(nullptr != GameInstance)
        {
            // The PreLogin is not called for the hosting player, so here we compare
            // PlayerCounter with the number of public connections - 1,
            // assuming the hosting player to be already logged in
            if(PlayerCounter == GameInstance->GetNumberOfPublicConnections() - 1)
            {
                ErrorMessage = TEXT("Server is full");

                return;
            }
            ++PlayerCounter;
        }
        else
        {
            ErrorMessage = TEXT("Internal error");

            return;
        }
    }
}

void ATRLobbyLevelGameMode::PostLogin(APlayerController* NewPlayer)
{
    Super::PostLogin(NewPlayer);

    UTRGameInstance* GameInstance = GetGameInstance<UTRGameInstance>();
    APlayerState* PlayerState = NewPlayer->GetPlayerState<APlayerState>();

    if(nullptr != PlayerState)
    {
        TR_LOG(this, Log, TEXT("Got login from %s"), *PlayerState->GetPlayerName());
    }
    else
    {
        TR_LOG(this, Warning, TEXT("Could not get player state!"));
    }


    if(nullptr != GameInstance)
    {
        ATRLobbyLevelPlayerController* Player = Cast<ATRLobbyLevelPlayerController>(NewPlayer);

        if(nullptr != Player)
        {
            if(ensureMsgf(Players.Num() < GameInstance->GetNumberOfPublicConnections(),
                          TEXT("Internal error! Got PostLogin request, while the server is already full!")))
            {
                Players.Add(Player);
                if(0 != Bots.Num())
                {
                    AAIController* AIController = Bots.Last();

                    Bots.Remove(AIController);
                    AIController->Destroy();
                }
            }
            else
            {
                GameSession->KickPlayer(NewPlayer, FText::FromString(TEXT("Server is full")));
            }
        }
        else
        {
            GameSession->KickPlayer(NewPlayer, FText::FromString(TEXT("Invalid player")));
        }
    }
    else
    {
        GameSession->KickPlayer(NewPlayer, FText::FromString(TEXT("Internal error")));
    }
}

void ATRLobbyLevelGameMode::Logout(AController* Exiting)
{
    Super::Logout(Exiting);
    if(HasAuthority())
    {
        ATRLobbyLevelPlayerController* Player = Cast<ATRLobbyLevelPlayerController>(Exiting);

        if(nullptr != Player)
        {
            Players.Remove(Player);
            --PlayerCounter;
        }
        CreateBots();
    }
}

AActor* ATRLobbyLevelGameMode::ChoosePlayerStart_Implementation(AController* Player)
{
    // TODO: Implement this
    return Super::ChoosePlayerStart_Implementation(Player);
}

bool ATRLobbyLevelGameMode::IsAllPlayersReady() const
{
    for(ATRLobbyLevelPlayerController* Player: Players)
    {
        ATRLobbyLevelPlayerState* PlayerState = Player->GetPlayerState<ATRLobbyLevelPlayerState>();

        if(ensureMsgf(nullptr != PlayerState, TEXT("Failed to get player state")))
        {
            if(!PlayerState->GetIsPlayerReady())
            {
                return false;
            }
        }
    }
    return true;
}

void ATRLobbyLevelGameMode::SetOnlineSessionEventReceiver()
{
     if(!bIsOnlineSessionEventReceiverSet)
    {
        if(SetOnlineSessionEventReceiver(this))
        {
            bIsOnlineSessionEventReceiverSet = true;
        }
        
    }
}

void ATRLobbyLevelGameMode::ResetOnlineSessionEventReceiver()
{
    if(bIsOnlineSessionEventReceiverSet)
    {
        if(SetOnlineSessionEventReceiver(nullptr))
        {
            bIsOnlineSessionEventReceiverSet = false;
        }
    }
}

bool ATRLobbyLevelGameMode::SetOnlineSessionEventReceiver(ATRLobbyLevelGameMode* OnlineSessionEventReceiver)
{
    UTRGameInstance* GameInstance = GetGameInstance<UTRGameInstance>();

    if(nullptr != GameInstance)
    {
        GameInstance->SetOnlineSessionEventReceiver(OnlineSessionEventReceiver);

        return true;
    }

    return false;
}

void ATRLobbyLevelGameMode::CreateBots()
{
    if(HasAuthority())
    {
        UTRGameInstance* GameInstance = GetGameInstance<UTRGameInstance>();

        if(ensureMsgf(nullptr != GameInstance, TEXT("Could not get game instance")))
        {
            UWorld* World = GetWorld();

            if(ensureMsgf(nullptr != World, TEXT("Could not get world")))
            {
                for(int32 Index = Players.Num(); Index < GameInstance->GetNumberOfPublicConnections(); ++Index)
                {
                    AAIController* AIController = World->SpawnActor<AAIController>();

                    if(ensureMsgf(nullptr != AIController, TEXT("Could not create AI controller")))
                    {
                        ATRLobbyLevelPlayerState* PlayerState = nullptr;

                        AIController->InitPlayerState();
                        PlayerState = AIController->GetPlayerState<ATRLobbyLevelPlayerState>();
                        if(ensureMsgf(nullptr != PlayerState, TEXT("Could not get player state")))
                        {
                            PlayerState->SetIsPlayerReady(true);
                            PlayerState->SetPlayerName(FString::Printf(TEXT("Bot%d"), Index));
                            PlayerState->SetIsPlayerNameSet(true);
                            PlayerState->bIsABot = true;
                            Bots.Add(AIController);
                        }
                        else
                        {
                            AIController->Destroy();
                        }
                    }
                }
            }
        }
    }
}
