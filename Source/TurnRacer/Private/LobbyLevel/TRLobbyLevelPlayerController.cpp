#include "LobbyLevel/TRLobbyLevelPlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "LobbyLevel/TRLobbyLevelGameMode.h"
#include "LobbyLevel/TRLobbyLevelPlayerState.h"
#include "TRGameInstance.h"
#include "TurnRacer/TurnRacer.h"

bool ATRLobbyLevelPlayerController::Ready_Validate()
{
    return true;
}

void ATRLobbyLevelPlayerController::Ready_Implementation()
{
    ATRLobbyLevelPlayerState* LobbyLevelPlayerState = GetPlayerState<ATRLobbyLevelPlayerState>();

    if(ensureMsgf(nullptr != LobbyLevelPlayerState, TEXT("Failed to get player state")))
    {
        if(!LobbyLevelPlayerState->GetIsPlayerReady())
        {
            ATRLobbyLevelGameMode* GameMode = Cast<ATRLobbyLevelGameMode>(UGameplayStatics::GetGameMode(this));

            if(ensureMsgf(nullptr != GameMode, TEXT("Could not get game mode")))
            {
                LobbyLevelPlayerState->SetIsPlayerReady(true);
                GameMode->PlayerReady();
            }
        }
    }
}

void ATRLobbyLevelPlayerController::Quit()
{
    UTRGameInstance* GameInstance = GetGameInstance<UTRGameInstance>();

    if(ensureMsgf(nullptr != GameInstance, TEXT("Could not get game instance")))
    {
        SetOnlineSessionEventReceiver();
        if(!GameInstance->DestroySession())
        {
            ResetOnlineSessionEventReceiver();
            BP_OnQuitFailed();
        }
    }
}

void ATRLobbyLevelPlayerController::BeginPlay()
{
    Super::BeginPlay();

    if(IsLocalController())
    {
        UTRGameInstance* GameInstance = GetGameInstance<UTRGameInstance>();

        if(ensureMsgf(nullptr != GameInstance, TEXT("Could not get game instance")))
        {
            Server_SetPlayerName(GameInstance->GetPlayerName());
        }
    }
}

//
// ITROnlineSessionEventReceiver interface
//

void ATRLobbyLevelPlayerController::OnCreateSessionSuccess_Implementation()
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnCreateSessionSuccess' received on lobby level player controller"));
}

void ATRLobbyLevelPlayerController::OnCreateSessionFailure_Implementation()
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnCreateSessionFailure' received on lobby level player controller"));
}

void ATRLobbyLevelPlayerController::OnStartSessionSuccess_Implementation()
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnStartSessionSuccess' received on lobby level player controller"));
}

void ATRLobbyLevelPlayerController::OnStartSessionFailure_Implementation()
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnStartSessionFailure' received on lobby level player controller"));
}

void ATRLobbyLevelPlayerController::OnFindSessionsSuccess_Implementation(const TArray<FString>& ServerNames)
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnFindSessionsSuccess' received on lobby level player controller"));
}

void ATRLobbyLevelPlayerController::OnFindSessionsFailure_Implementation()
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnFindSessionsFailure' received on lobby level player controller"));
}

void ATRLobbyLevelPlayerController::OnCancelFindSessionsSuccess_Implementation()
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnCancelFindSessionsSuccess' received on lobby level player controller"));
}

void ATRLobbyLevelPlayerController::OnCancelFindSessionsFailure_Implementation()
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnCancelFindSessionsFailure' received on lobby level player controller"));
}

void ATRLobbyLevelPlayerController::OnJoinSessionSuccess_Implementation(const FString& ConnectionString)
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnJoinSessionSuccess' received on lobby level player controller"));
}

void ATRLobbyLevelPlayerController::OnJoinSessionFailure_Implementation()
{
    TR_LOG(this, Error, TEXT("Invalid callback 'OnJoinSessionFailure' received on lobby level player controller"));
}

void ATRLobbyLevelPlayerController::OnDestroySessionSuccess_Implementation()
{
    ResetOnlineSessionEventReceiver();
    BP_OnQuitSucceeded();
}

void ATRLobbyLevelPlayerController::OnDestroySessionFailure_Implementation()
{
    ResetOnlineSessionEventReceiver();
    BP_OnQuitFailed();
}

void ATRLobbyLevelPlayerController::SetOnlineSessionEventReceiver()
{
     if(!bIsOnlineSessionEventReceiverSet)
    {
        if(SetOnlineSessionEventReceiver(this))
        {
            bIsOnlineSessionEventReceiverSet = true;
        }
        
    }
}

void ATRLobbyLevelPlayerController::ResetOnlineSessionEventReceiver()
{
    if(bIsOnlineSessionEventReceiverSet)
    {
        if(SetOnlineSessionEventReceiver(nullptr))
        {
            bIsOnlineSessionEventReceiverSet = false;
        }
    }
}

bool ATRLobbyLevelPlayerController::SetOnlineSessionEventReceiver(ATRLobbyLevelPlayerController* OnlineSessionEventReceiver)
{
    UTRGameInstance* GameInstance = GetGameInstance<UTRGameInstance>();

    if(nullptr != GameInstance)
    {
        GameInstance->SetOnlineSessionEventReceiver(OnlineSessionEventReceiver);

        return true;
    }

    return false;
}

bool ATRLobbyLevelPlayerController::Server_SetPlayerName_Validate(const FString& PlayerName)
{
    return true;
}

void ATRLobbyLevelPlayerController::Server_SetPlayerName_Implementation(const FString& PlayerName)
{
    ATRLobbyLevelPlayerState* LobbyLevelPlayerState = GetPlayerState<ATRLobbyLevelPlayerState>();

    if(ensureMsgf(nullptr != LobbyLevelPlayerState, TEXT("Could not get player state")))
    {
        LobbyLevelPlayerState->SetPlayerName(PlayerName);
        LobbyLevelPlayerState->SetIsPlayerNameSet(true);
    }
}

void ATRLobbyLevelPlayerController::Client_OnGameStarts_Implementation()
{
    BP_OnGameStarts();
}

void ATRLobbyLevelPlayerController::Client_OnGameStartFailed_Implementation()
{
    BP_OnGameStartFailed();
}
