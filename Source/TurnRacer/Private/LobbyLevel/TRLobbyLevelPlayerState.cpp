#include "LobbyLevel/TRLobbyLevelPlayerState.h"
#include "Net/UnrealNetwork.h"

bool ATRLobbyLevelPlayerState::GetIsPlayerNameSet() const
{
    return bIsPlayerNameSet;
}

void ATRLobbyLevelPlayerState::SetIsPlayerNameSet(bool bInIsPlayerNameSet)
{
    bIsPlayerNameSet = bInIsPlayerNameSet;
}

bool ATRLobbyLevelPlayerState::GetIsPlayerReady() const
{
    return bIsPlayerReady;
}

void ATRLobbyLevelPlayerState::SetIsPlayerReady(bool bInIsPlayerReady)
{
    bIsPlayerReady = bInIsPlayerReady;
}

void ATRLobbyLevelPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ATRLobbyLevelPlayerState, bIsPlayerNameSet);
    DOREPLIFETIME(ATRLobbyLevelPlayerState, bIsPlayerReady);
}
