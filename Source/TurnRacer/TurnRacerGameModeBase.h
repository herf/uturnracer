#pragma once
#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TurnRacerGameModeBase.generated.h"

UCLASS()
class TURNRACER_API ATurnRacerGameModeBase : public AGameModeBase
{
    GENERATED_BODY()

public:
    ATurnRacerGameModeBase();
};
