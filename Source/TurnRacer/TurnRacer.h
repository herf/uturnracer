#pragma once
#include "CoreMinimal.h"

class AActor;

AActor* NullActor = nullptr;

DECLARE_LOG_CATEGORY_EXTERN(LogTR, Log, All);

#define TR_LOG(Actor, Verbosity, Format, ...) \
    { \
        ENetMode NetMode = Actor != nullptr ? Actor->GetNetMode() : NM_MAX; \
        switch (NetMode) \
        { \
            case NM_Standalone: \
            case NM_DedicatedServer: \
            case NM_ListenServer: \
                UE_LOG(LogTR, Verbosity, TEXT("[SERVER]: ") TEXT(__FUNCTION__) TEXT(" -- ") Format, ##__VA_ARGS__); \
                break; \
            case NM_Client: \
                UE_LOG(LogTR, Verbosity, TEXT("[CLIENT]: ") TEXT(__FUNCTION__) TEXT(" -- ") Format, ##__VA_ARGS__); \
                break; \
            default: \
                UE_LOG(LogTR, Verbosity, TEXT("[UNKNOWN]: ") TEXT(__FUNCTION__) TEXT(" -- ") Format, ##__VA_ARGS__); \
                break; \
        }; \
    }

#define TR_LOG_METHOD(Actor) \
    { \
        ENetMode NetMode = Actor != nullptr ? Actor->GetNetMode() : NM_MAX; \
        switch (NetMode) \
        { \
            case NM_Standalone: \
            case NM_DedicatedServer: \
            case NM_ListenServer: \
                UE_LOG(LogTR, Log, TEXT("[SERVER]: ") TEXT(__FUNCTION__)); \
                break; \
            case NM_Client: \
                UE_LOG(LogTR, Log, TEXT("[CLIENT]: ") TEXT(__FUNCTION__)); \
                break; \
            default: \
                UE_LOG(LogTR, Log, TEXT("[UNKNOWN]: ") TEXT(__FUNCTION__)); \
                break; \
        }; \
    }
