#include "TurnRacer.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TurnRacer, "TurnRacer" );

DEFINE_LOG_CATEGORY(LogTR);
