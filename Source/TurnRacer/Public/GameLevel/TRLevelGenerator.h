#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TRGameInstance.h"
#include "TRLevelGenerator.generated.h"

class UProceduralMeshComponent;

USTRUCT()
struct FReplicatedLevelColumn
{
    GENERATED_BODY()

    UPROPERTY()
    TArray<bool> LevelColumn;
};

USTRUCT()
struct FReplicatedLevel
{
    GENERATED_BODY()

    UPROPERTY()
    TArray<FReplicatedLevelColumn> Level;
};

UCLASS()
class TURNRACER_API ATRLevelGenerator: public AActor
{
    GENERATED_BODY()

public:
    ATRLevelGenerator();

    UFUNCTION(BlueprintGetter)
    ELevelSize GetLevelSize() const;
    UFUNCTION(BlueprintSetter)
    void SetLevelSize(ELevelSize InLevelSize);

    UFUNCTION(BlueprintGetter)
    int32 GetRoadSize() const;
    UFUNCTION(BlueprintSetter)
    void SetRoadSize(int32 InRoadSize);

    UFUNCTION(BlueprintGetter)
    int32 GetRefineLevel() const;
    UFUNCTION(BlueprintSetter)
    void SetRefineLevel(int32 InRefineLevel);

    UFUNCTION(BlueprintGetter)
    UMaterialInterface* GetMaterial() const;
    UFUNCTION(BlueprintSetter)
    void SetMaterial(UMaterialInterface* InMaterial);

    void GenerateLevel();

    TArray<FVector> GetStartPositions() const;

    FVector GetMiddlePoint() const;

protected:
    virtual void BeginPlay() override;
    virtual void Tick(float DeltaTime) override;

    ELevelSize LevelSize = ELevelSize::LS_Large;
    // Defines the additional width of the road in meters. Zero means the road will be one meter only
    int32 RoadSize = 1;
    // Defines how many times the road between the starting points will be generated
    int32 RefineLevel = 1;

    UPROPERTY(EditDefaultsOnly, ReplicatedUsing=OnRep_Material)
    UMaterialInterface* Material = nullptr;
    UPROPERTY(EditDefaultsOnly)
    UProceduralMeshComponent* Mesh = nullptr;

private:
    UFUNCTION()
    void OnRep_ReplicatedLevel();
    UFUNCTION()
    void OnRep_Material();

    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

    // Returns the size of the level in meters based on the provided LevelSize enum
    int32 GetSize() const;
    // Generates the whole level
    void GenerateLevel(const FVector2D& From, const FVector2D& To);
    // Adds the given position to the level, and also handles "neighbouring" based on the given RoadSize
    void AddToLevel(const FVector2D& Position);
    // Rounds the given vector's components to integers
    FVector2D Round(const FVector2D& Vector) const;
    // Checks if the given position is valid within the level
    bool IsValid(const FVector2D& Position) const;
    // Calculates the next position towards "To" from "Current". Adds randomization if "bRandomize" is set to true
    FVector2D CalculateNewPosition(const FVector2D& Current, const FVector2D& To, bool bRandomize);
    // Creates the mesh based on the generated level
    void CreateMesh();

    // Sets up starting points and the middle point
    void SetPoints();

    TArray<TArray<bool>> Level;
    TArray<FVector2D> StartPoints;
    FVector2D MiddlePoint;
    UPROPERTY(ReplicatedUsing=OnRep_ReplicatedLevel)
    FReplicatedLevel ReplicatedLevel;
};
