#pragma once
#include "CoreMinimal.h"
#include "TurnRacer/TurnRacerGameModeBase.h"
#include "TRGameLevelGameMode.generated.h"

class ATRLevelGenerator;
class ATRAIController;

UCLASS()
class TURNRACER_API ATRGameLevelGameMode : public ATurnRacerGameModeBase
{
    GENERATED_BODY()

private:
    virtual void BeginPlay() override;
    virtual void PostLogin(APlayerController* NewPlayer) override;

    FRotator GetPawnRotation(APawn* Pawn) const;

    // Defines the size of the level in meters
    UPROPERTY(EditAnywhere, Category="LevelGeneration", Meta=(ClampMin=32, ClampMax=128))
    int32 Size = 128;
    // Defines the additional width of the road in meters. Zero means the road will be one meter only
    UPROPERTY(EditAnywhere, Category="LevelGeneration", Meta=(ClampMin=0, ClampMax=2))
    int32 RoadSize = 1;
    // Defines how many times the road between the starting points will be generated
    UPROPERTY(EditAnywhere, Category="LevelGeneration", Meta=(ClampMin=1, ClampMax=10))
    int32 RefineLevel = 1;
    // The material to be used for the level
    UPROPERTY(EditDefaultsOnly, Category="LevelGeneration")
    UMaterialInterface* LevelMaterial;
    // Actor to be placed at each start position
    UPROPERTY(EditDefaultsOnly)
    TSubclassOf<AActor> StartPositionClass = nullptr;
    // Actor to be used as players or AI controlled players
    UPROPERTY(EditDefaultsOnly)
    TSubclassOf<APawn> PlayerClass = nullptr;
    // AIController to control the bots
    UPROPERTY(EditDefaultsOnly)
    TSubclassOf<ATRAIController> AiControllerClass = nullptr;

    UPROPERTY()
    ATRLevelGenerator* LevelGenerator;

    UPROPERTY()
    TMap<FVector, APawn*> PlayerPositions;
};
