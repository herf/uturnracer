#pragma once
#include "CoreMinimal.h"
#include "TRGameInstance.h"
#include "TROnlineSessionEventReceiver.h"
#include "Blueprint/UserWidget.h"
#include "TRMainMenuUserWidget.generated.h"

UCLASS()
class TURNRACER_API UTRMainMenuUserWidget: public UUserWidget, public ITROnlineSessionEventReceiver
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable)
    void Host(const FString& ServerName,
              const FString& PlayerName,
              int32 NumberOfPublicConnections,
              EOnlineSubsystemType OnlineSubsystem,
              ELevelSize LevelSize);

    UFUNCTION(BlueprintCallable)
    void FindSessions();
    UFUNCTION(BlueprintCallable)
    void CancelFindSessions();

    UFUNCTION(BlueprintCallable)
    void JoinSession(const FString& ServerName,
                     const FString& PlayerName);

    UFUNCTION(BlueprintCallable)
    void DestroySession();

protected:
    //
    // Host related BP callbacks
    //
    UFUNCTION(BlueprintImplementableEvent, meta=(DisplayName="OnHostSucceeded"))
    void BP_OnHostSucceeded();
    UFUNCTION(BlueprintImplementableEvent, meta=(DisplayName="OnHostFailed"))
    void BP_OnHostFailed();

    //
    // Find Sessions related BP callbacks
    //
    UFUNCTION(BlueprintImplementableEvent, meta=(DisplayName="OnFindSessionsSucceeded"))
    void BP_OnFindSessionsSucceeded(const TArray<FString>& ServerNames);
    UFUNCTION(BlueprintImplementableEvent, meta=(DisplayName="OnFindSessionsFailed"))
    void BP_OnFindSessionsFailed();

    //
    // Cancel Find Sessions related BP callbacks
    //
    UFUNCTION(BlueprintImplementableEvent, meta=(DisplayName="OnCancelFindSessionsSucceeded"))
    void BP_OnCancelFindSessionsSucceeded();
    UFUNCTION(BlueprintImplementableEvent, meta=(DisplayName="OnCancelFindSessionsFailed"))
    void BP_OnCancelFindSessionsFailed();

    //
    // Join Session related BP callbacks
    //
    UFUNCTION(BlueprintImplementableEvent, meta=(DisplayName="OnJoinSessionSucceeded"))
    void BP_OnJoinSessionSucceeded();
    UFUNCTION(BlueprintImplementableEvent, meta=(DisplayName="OnJoinSessionFailed"))
    void BP_OnJoinSessionFailed();

    //
    // Destroy Session related BP callbacks
    //
    UFUNCTION(BlueprintImplementableEvent, meta=(DisplayName="OnDestroySessionSucceeded"))
    void BP_OnDestroySessionSucceeded();
    UFUNCTION(BlueprintImplementableEvent, meta=(DisplayName="OnDestroySessionFailed"))
    void BP_OnDestroySessionFailed();

private:
    //
    // ITROnlineSessionEventReceiver interface
    //
    virtual void OnCreateSessionSuccess_Implementation() override;
    virtual void OnCreateSessionFailure_Implementation() override;

    virtual void OnStartSessionSuccess_Implementation() override;
    virtual void OnStartSessionFailure_Implementation() override;

    virtual void OnFindSessionsSuccess_Implementation(const TArray<FString>& ServerNames) override;
    virtual void OnFindSessionsFailure_Implementation() override;

    virtual void OnCancelFindSessionsSuccess_Implementation() override;
    virtual void OnCancelFindSessionsFailure_Implementation() override;

    virtual void OnJoinSessionSuccess_Implementation(const FString& ConnectionString) override;
    virtual void OnJoinSessionFailure_Implementation() override;

    virtual void OnDestroySessionSuccess_Implementation() override;
    virtual void OnDestroySessionFailure_Implementation() override;

    //
    // Helper methods
    //
    void SetOnlineSessionEventReceiver();
    void ResetOnlineSessionEventReceiver();
    bool SetOnlineSessionEventReceiver(UTRMainMenuUserWidget* OnlineSessionEventReceiver);

    bool bIsOnlineSessionEventReceiverSet = false;
};
