#pragma once
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TRLobbyMenuUserWidget.generated.h"

UCLASS()
class TURNRACER_API UTRLobbyMenuUserWidget: public UUserWidget
{
    GENERATED_BODY()

protected:
    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, meta=(DisplayName="OnGameStarts"))
    void BP_OnGameStarts();

    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, meta=(DisplayName="OnGameStartFailed"))
    void BP_OnGameStartFailed();
};
