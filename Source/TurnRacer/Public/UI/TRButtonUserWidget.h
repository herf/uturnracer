#pragma once
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "TRButtonUserWidget.generated.h"

class UTextBlock;

UCLASS()
class TURNRACER_API UTRButtonUserWidget : public UUserWidget
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintSetter)
    void SetText(const FText& InText);

protected:
    virtual void NativePreConstruct() override;

    UFUNCTION()
    void OnClick();
    UFUNCTION()
    void OnPress();
    UFUNCTION()
    void OnRelease();
    UFUNCTION()
    void OnHover();
    UFUNCTION()
    void OnUnhover();

    UPROPERTY(meta=(BindWidget))
    UButton* Button;
    UPROPERTY(meta=(BindWidget))
    UTextBlock* TextBlock;
    UPROPERTY(EditAnywhere, BlueprintSetter=SetText)
    FText Text;
    UPROPERTY(BlueprintAssignable)
    FOnButtonClickedEvent OnClicked;
    UPROPERTY(BlueprintAssignable)
    FOnButtonPressedEvent OnPressed;
    UPROPERTY(BlueprintAssignable)
    FOnButtonReleasedEvent OnReleased;
    UPROPERTY(BlueprintAssignable)
    FOnButtonHoverEvent OnHovered;
    UPROPERTY(BlueprintAssignable)
    FOnButtonHoverEvent OnUnhovered;
};
