#pragma once
#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "OnlineSessionSettings.h"
#include "OnlineSubsystem.h"
#include "TRGameInstance.generated.h"

class ITROnlineSessionEventReceiver;
class UNetDriver;

UENUM(BlueprintType)
enum class ELevelSize: uint8
{
    LS_Small UMETA(DisplayName = "Small"),
    LS_Medium UMETA(DisplayName = "Medium"),
    LS_Large UMETA(DisplayName = "Large"),
};

UENUM(BlueprintType)
enum class EOnlineSubsystemType: uint8
{
    OSST_Lan UMETA(DisplayName = "LAN"),
    OSST_Steam UMETA(DisplayName = "Steam"),
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnNetworkFailureDelegate, const FString&, ErrorMessage);

UCLASS()
class TURNRACER_API UTRGameInstance : public UGameInstance
{
    GENERATED_BODY()

public:
    static const FName SERVER_NAME_KEY;

    UFUNCTION(BlueprintCallable)
    void SetOnlineSessionEventReceiver(UObject* InOnlineSessionEventReceiver);

    UFUNCTION(BlueprintCallable)
    bool CreateSession(const FString& ServerName,
                       const FString& InPlayerName,
                       int32 InNumberOfPublicConnections,
                       EOnlineSubsystemType OnlineSubsystem,
                       ELevelSize InLevelSize);
    UFUNCTION(BlueprintCallable)
    bool StartSession();

    UFUNCTION(BlueprintCallable)
    bool FindSessions();
    UFUNCTION(BlueprintCallable)
    bool CancelFindSessions();
    UFUNCTION(BlueprintCallable)
    bool Join(const FString& ServerName,
              const FString& InPlayerName);

    UFUNCTION(BlueprintCallable)
    bool DestroySession();

    UFUNCTION(BlueprintGetter)
    int32 GetNumberOfPublicConnections() const;

    UFUNCTION(BlueprintGetter)
    int32 GetNumberOfConnectedPlayers() const;

    void SetNumberOfConnectedPlayers(int32 InNumberOfConnectedPlayers);

    UFUNCTION(BlueprintGetter)
    const FString& GetPlayerName() const;
    UFUNCTION(BlueprintSetter)
    void SetPlayerName(const FString& InPlayerName);

    ELevelSize GetLevelSize() const;

protected:
    using FOnlineSessionSearchPtr = TSharedPtr<FOnlineSessionSearch>;

    template<typename InterfaceType>
    static bool ImplementsInterface(UObject* Object)
    {
        return nullptr != Object && Object->GetClass()->ImplementsInterface(InterfaceType::StaticClass());
    }

    // Overrides
    virtual void Init() override;

    // Listen for network errors
    void OnNetworkFailure(UWorld* World,
                          UNetDriver* NetDriver,
                          ENetworkFailure::Type FailureType,
                          const FString& ErrorMessage);

    // Helper methods
    bool CheckSessionInterface() const;
    bool CheckOnlineSessionEventReceiver() const;
    bool CheckSessionCreated() const;

    // Host related callbacks
    void OnCreateSessionComplete(FName SessionName, bool bWasSuccessful);
    void OnStartSessionComplete(FName SessionName, bool bWasSuccessful);

    // Join related callbacks
    void OnFindSessionsComplete(bool bWasSuccessful);
    void OnCancelFindSessionsComplete(bool bWasSuccessful);
    void OnJoinSessionsComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);

    // Destroy related callbacks
    void OnDestroySessionComplete(FName SessionName, bool bWasSuccessful);

    IOnlineSessionPtr SessionInterface;
    FOnlineSessionSettings SessionSettings;
    FOnlineSessionSearchPtr SessionSearch;
    // Host related delegate handles
    FDelegateHandle OnCreateSessionDelegateHandle;
    FDelegateHandle OnStartSessionDelegateHandle;
    // Join related delegate handles
    FDelegateHandle OnFindSessionsDelegateHandle;
    FDelegateHandle OnCancelFindSessionsDelegateHandle;
    FDelegateHandle OnJoinSessionDelegateHandle;
    // Destroy related delegate handles
    FDelegateHandle OnDestroySessionDelegateHandle;

    UObject* OnlineSessionEventReceiver;

    UPROPERTY(BlueprintGetter=GetNumberOfPublicConnections)
    int32 NumberOfPublicConnections;
    UPROPERTY(BlueprintGetter=GetNumberOfConnectedPlayers)
    int32 NumberOfConnectedPlayers;

    UPROPERTY(BlueprintReadWrite)
    ELevelSize LevelSize;

    UPROPERTY(BlueprintGetter=GetPlayerName, BlueprintSetter=SetPlayerName)
    FString PlayerName;

    UPROPERTY(BlueprintAssignable)
    FOnNetworkFailureDelegate OnNetworkFailureDelegate;
};
