#pragma once
#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "TRLobbyLevelPlayerState.generated.h"

UCLASS()
class TURNRACER_API ATRLobbyLevelPlayerState: public APlayerState
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintGetter)
    bool GetIsPlayerNameSet() const;
    UFUNCTION(BlueprintSetter)
    void SetIsPlayerNameSet(bool bInIsPlayerNameSet);

    UFUNCTION(BlueprintGetter)
    bool GetIsPlayerReady() const;
    UFUNCTION(BlueprintSetter)
    void SetIsPlayerReady(bool bInIsPlayerReady);

protected:
    virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;

    UPROPERTY(BlueprintGetter=GetIsPlayerNameSet, BlueprintSetter=SetIsPlayerNameSet, Replicated)
    bool bIsPlayerNameSet = false;
    UPROPERTY(BlueprintGetter=GetIsPlayerReady, BlueprintSetter=SetIsPlayerReady, Replicated)
    bool bIsPlayerReady = false;
};
