#pragma once
#include "CoreMinimal.h"
#include "TurnRacer/TurnRacerGameModeBase.h"
#include "TROnlineSessionEventReceiver.h"
#include "TRLobbyLevelPlayerController.h"
#include "AIController.h"
#include "TRLobbyLevelGameMode.generated.h"

UCLASS()
class TURNRACER_API ATRLobbyLevelGameMode: public ATurnRacerGameModeBase, public ITROnlineSessionEventReceiver
{
    GENERATED_BODY()

public:
    ATRLobbyLevelGameMode();

    UFUNCTION(BlueprintCallable)
    void PlayerReady();

    UFUNCTION(BlueprintCallable)
    void StartGame();

private:
    //
    // AActor overrides
    //
    virtual void BeginPlay() override;

    //
    // ITROnlineSessionEventReceiver interface
    //
    virtual void OnCreateSessionSuccess_Implementation() override;
    virtual void OnCreateSessionFailure_Implementation() override;

    virtual void OnStartSessionSuccess_Implementation() override;
    virtual void OnStartSessionFailure_Implementation() override;

    virtual void OnFindSessionsSuccess_Implementation(const TArray<FString>& ServerNames) override;
    virtual void OnFindSessionsFailure_Implementation() override;

    virtual void OnCancelFindSessionsSuccess_Implementation() override;
    virtual void OnCancelFindSessionsFailure_Implementation() override;

    virtual void OnJoinSessionSuccess_Implementation(const FString& ConnectionString) override;
    virtual void OnJoinSessionFailure_Implementation() override;

    virtual void OnDestroySessionSuccess_Implementation() override;
    virtual void OnDestroySessionFailure_Implementation() override;

    //
    // AGameModeBase overrides
    //
    virtual void PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage) override;
    virtual void PostLogin(APlayerController* NewPlayer) override;
    virtual void Logout(AController* Exiting) override;
    virtual AActor* ChoosePlayerStart_Implementation(AController* Player) override;

    //
    // Helper methods
    //
    bool IsAllPlayersReady() const;
    // TODO: These three methods are basically the same as in UTRMainMenuUserWidget
    void SetOnlineSessionEventReceiver();
    void ResetOnlineSessionEventReceiver();
    bool SetOnlineSessionEventReceiver(ATRLobbyLevelGameMode* OnlineSessionEventReceiver);
    void CreateBots();

    UPROPERTY()
    TArray<ATRLobbyLevelPlayerController*> Players;
    UPROPERTY()
    TArray<AAIController*> Bots;

    int32 PlayerCounter = 0;
    bool bIsOnlineSessionEventReceiverSet = false;
};
