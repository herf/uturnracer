#pragma once
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TROnlineSessionEventReceiver.h"
#include "TRLobbyLevelPlayerController.generated.h"

UCLASS()
class TURNRACER_API ATRLobbyLevelPlayerController: public APlayerController, public ITROnlineSessionEventReceiver
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, Server, WithValidation, Reliable)
    void Ready();

    UFUNCTION(BlueprintCallable)
    void Quit();

    UFUNCTION(Client, Unreliable)
    void Client_OnGameStarts();

    UFUNCTION(Client, Reliable)
    void Client_OnGameStartFailed();

protected:
    virtual void BeginPlay() override;

    //
    // ITROnlineSessionEventReceiver interface
    //
    virtual void OnCreateSessionSuccess_Implementation() override;
    virtual void OnCreateSessionFailure_Implementation() override;

    virtual void OnStartSessionSuccess_Implementation() override;
    virtual void OnStartSessionFailure_Implementation() override;

    virtual void OnFindSessionsSuccess_Implementation(const TArray<FString>& ServerNames) override;
    virtual void OnFindSessionsFailure_Implementation() override;

    virtual void OnCancelFindSessionsSuccess_Implementation() override;
    virtual void OnCancelFindSessionsFailure_Implementation() override;

    virtual void OnJoinSessionSuccess_Implementation(const FString& ConnectionString) override;
    virtual void OnJoinSessionFailure_Implementation() override;

    virtual void OnDestroySessionSuccess_Implementation() override;
    virtual void OnDestroySessionFailure_Implementation() override;

    // TODO: These three methods are basically the same as in UTRMainMenuUserWidget
    void SetOnlineSessionEventReceiver();
    void ResetOnlineSessionEventReceiver();
    bool SetOnlineSessionEventReceiver(ATRLobbyLevelPlayerController* OnlineSessionEventReceiver);

    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, meta=(DisplayName="OnGameStarts"))
    void BP_OnGameStarts();

    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, meta=(DisplayName="OnGameStartFailed"))
    void BP_OnGameStartFailed();

    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, meta=(DisplayName="OnQuitSucceeded"))
    void BP_OnQuitSucceeded();
    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, meta=(DisplayName="OnQuitFailed"))
    void BP_OnQuitFailed();

    UFUNCTION(Server, Reliable, WithValidation)
    void Server_SetPlayerName(const FString& PlayerName);

    bool bIsOnlineSessionEventReceiverSet = false;
};
