#pragma once
#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TROnlineSessionEventReceiver.generated.h"

UINTERFACE(MinimalAPI)
class UTROnlineSessionEventReceiver: public UInterface
{
    GENERATED_BODY()
};

class TURNRACER_API ITROnlineSessionEventReceiver
{
    GENERATED_BODY()

public:
    //
    // Create session callbacks
    // 
    UFUNCTION(BlueprintNativeEvent)
    void OnCreateSessionSuccess();
    virtual void OnCreateSessionSuccess_Implementation() = 0;

    UFUNCTION(BlueprintNativeEvent)
    void OnCreateSessionFailure();
    virtual void OnCreateSessionFailure_Implementation() = 0;

    //
    // Start session callbacks
    //
    UFUNCTION(BlueprintNativeEvent)
    void OnStartSessionSuccess();
    virtual void OnStartSessionSuccess_Implementation() = 0;

    UFUNCTION(BlueprintNativeEvent)
    void OnStartSessionFailure();
    virtual void OnStartSessionFailure_Implementation() = 0;

    //
    // Find sessions callbacks
    //
    UFUNCTION(BlueprintNativeEvent)
    void OnFindSessionsSuccess(const TArray<FString>& ServerNames);
    virtual void OnFindSessionsSuccess_Implementation(const TArray<FString>& ServerNames) = 0;

    UFUNCTION(BlueprintNativeEvent)
    void OnFindSessionsFailure();
    virtual void OnFindSessionsFailure_Implementation() = 0;

    //
    // Cancel find sessions callbacks
    //
    UFUNCTION(BlueprintNativeEvent)
    void OnCancelFindSessionsSuccess();
    virtual void OnCancelFindSessionsSuccess_Implementation() = 0;

    UFUNCTION(BlueprintNativeEvent)
    void OnCancelFindSessionsFailure();
    virtual void OnCancelFindSessionsFailure_Implementation() = 0;

    //
    // Join session callbacks
    //
    UFUNCTION(BlueprintNativeEvent)
    void OnJoinSessionSuccess(const FString& ConnectionString);
    virtual void OnJoinSessionSuccess_Implementation(const FString& ConnectionString) = 0;

    UFUNCTION(BlueprintNativeEvent)
    void OnJoinSessionFailure();
    virtual void OnJoinSessionFailure_Implementation() = 0;

    //
    // Destroy session callbacks
    //
    UFUNCTION(BlueprintNativeEvent)
    void OnDestroySessionSuccess();
    virtual void OnDestroySessionSuccess_Implementation() = 0;

    UFUNCTION(BlueprintNativeEvent)
    void OnDestroySessionFailure();
    virtual void OnDestroySessionFailure_Implementation() = 0;
};
